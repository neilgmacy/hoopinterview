//
//  NetworkActivityProvider.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import Foundation

class NetworkActivityProvider {
  private let activitiesUrl = URL(string: "http://files.hoop.co.uk/test.json")
  private let ephemeralSession = URLSession(configuration: .ephemeral)
  private var dataTask: URLSessionDataTask?
  
  func loadData(_ finishedLoading: @escaping (_ activities: Array<Activity>?) -> Void) {
    if (dataTask != nil) {
      //make sure there's no ongoing task
      dataTask?.cancel()
    }
    
    dataTask = ephemeralSession.dataTask(with: activitiesUrl!) { data, response, error in
      if let error = error {
        print(error.localizedDescription)
        finishedLoading(nil)
      } else if let httpResponse = response as? HTTPURLResponse {
        if (httpResponse.statusCode == 200) {
          let parsedActivities = self.parseActivities(from: data!)
          finishedLoading(parsedActivities)
        } else {
          finishedLoading(nil)
        }
      }
    }
    dataTask?.resume()
  }
  
  private func parseActivities(from data: Data) -> Array<Activity> {
    var parsedActivities = Array<Activity>()
    if let activities = try? JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? Array<[String: Any]> {
      for item in activities! {
        let activity = Activity(json: item)
        parsedActivities.append(activity!)
      }
    }
    
    return parsedActivities
  }
}
