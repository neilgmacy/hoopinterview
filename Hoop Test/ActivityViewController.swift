//
//  ActivityViewController.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

class ActivityViewController : UIViewController {
  var activity: Activity! {
    didSet {
      self.activityView.activity = activity
    }
  }
  
  var coordinatorDelegate: AppCoordinatorDelegate? {
    didSet {
      activityView.coordinatorDelegate = coordinatorDelegate
    }
  }
  
  private let scrollView = UIScrollView()
  private let activityView = ActivityView()
  
  override func viewDidLoad() {
    view.backgroundColor = .white
    view.addSubview(scrollView)
    
    //AutoLayout constraints
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
    scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
    scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    scrollView.alwaysBounceVertical = true
    
    scrollView.addSubview(activityView)
    activityView.translatesAutoresizingMaskIntoConstraints = false
    activityView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
    activityView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    activityView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    scrollView.contentSize = activityView.frame.size
  }
}
