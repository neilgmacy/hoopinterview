# README

## Activities app

### Design
The app has two main ViewControllers: the `HomeViewController`, showing the list of activities, and the `ActivityViewController` showing information about a selected activity. Tapping an activity in the home screen will take you to the activity screen.

The colour of activities alternates between green, blue, red and yellow. I chose these to get a bit more personality in the app, and I thought the colours seemed appropriately fun for a family activities app.

In the activity detail screen, the image that was shown as a thumbnail is made full width at the top of the `ActivityView`, to bring some life to a fairly text-heavy screen. The name and main description of the activity are bolder to give them emphasis and draw more attention, while the other activity metadata is lighter. At the bottom is the address and a map preview of the location. There's also a button letting you view the map in full detail in Maps.app, which also allows the user to get directions.

### Architecture
The app uses Coordinators ([more information in the original blog post here](http://khanlou.com/2015/10/coordinators-redux/)) to separate app routing logic from view logic. When it's time to move through the app, or open a different app, all the work is delegated to the `AppCoordinator`. Ideally I should have more than one coordinator and that would be my next refactor if the app were to grow.

The app's ViewControllers have very little code in them, other than code that tells their subviews what to do (`HomeViewController` mostly controls its `UITableView`. `ActivityViewController` mostly just lays out its scroll view and content view.). Views know even less; they simply contain the subviews that compose them, AutoLayout code to arrange these subviews, and a required `Activity` property. The `Activity` property has an overloaded setter which will update the subviews for the new activity.

I've written all of the views in code. There's no storyboard in the project other than the one required for the launch image. I'm using AutoLayout and LayoutAnchors to layout the code in views, although they're a bit verbose! There are no 3rd party dependencies in the app, however I would add something like [SnapKit](http://snapkit.io) to simplify the layout code.

### Data
The Coordinators in the app are also responsible for getting data and passing it to the appropriate ViewController. I'm using a top level class called `ActivityProvider` to act as an intermediate between the Coordinators and the data sources. At the moment, there's just a direct connection to the API but the main Provider of data could be expanded to decide between going to the API, local storage or an in-memory cache.

The model data from the API is parsed using iOS's built in `JSONSerialization` class. This then gets passed to the relevant model object's constructor to create the object.