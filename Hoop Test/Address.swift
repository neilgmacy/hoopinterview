//
//  Address.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 14/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import Foundation

struct Address {
  let streetName: String
  let town: String
  let postcode: String
  let country: String
  let latitude: Double
  let longitude: Double
  
  var formattedAddress: String {
    get {
      return "\(streetName)\n\(town)\n\(postcode)\n\(country)"
    }
  }
  
  init?(json: [String: Any]) {
    guard let streetName = json["streetName"] as? String,
      let town = json["town"] as? String,
      let postcode = json["postcode"] as? String,
      let country = json["country"] as? String,
      let latitude = json["latitude"] as? Double,
      let longitude = json["longitude"] as? Double
      else {
        return nil
    }
    
    self.streetName = streetName
    self.town = town
    self.postcode = postcode
    self.country = country
    self.latitude = latitude
    self.longitude = longitude
  }
}
