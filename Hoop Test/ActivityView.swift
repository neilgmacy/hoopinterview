//
//  ActivityView.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

class ActivityView: UIView {
  var activity: Activity! {
    didSet {
      activityName.text = activity.name
      activityDescription.text = activity.description
      activityCategory.text = activity.category
      ageRange.text = activity.ageRange
      dateAndTime.text = activity.dateTimeString
      
      let thumbnailUrlString = activity.imageThumbnailUrlString
      if let thumbnailUrl = URL(string: thumbnailUrlString) {
        heroImage.downloadImage(url: thumbnailUrl)
      }
      
      location.activity = activity
    }
  }
  
  var coordinatorDelegate: AppCoordinatorDelegate? {
    didSet {
      location.coordinatorDelegate = coordinatorDelegate
    }
  }
  
  private let heroImage: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  private let activityName: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.font = .preferredFont(forTextStyle: .title2)
    label.numberOfLines = 0
    return label
  }()
  
  private let activityDescription: UILabel = {
    let label = UILabel()
    label.textColor = .black
    label.font = .preferredFont(forTextStyle: .headline)
    label.numberOfLines = 0
    return label
  }()
  
  private let activityCategory: UILabel = {
    let label = UILabel()
    label.textColor = .darkGray
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let ageRange: UILabel = {
    let label = UILabel()
    label.textColor = .darkGray
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let dateAndTime: UILabel = {
    let label = UILabel()
    label.textColor = .darkGray
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let location = LocationView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    self.addSubview(heroImage)
    self.addSubview(activityName)
    self.addSubview(activityDescription)
    self.addSubview(activityCategory)
    self.addSubview(ageRange)
    self.addSubview(dateAndTime)
    self.addSubview(location)
    
    //AutoLayout constraints
    heroImage.translatesAutoresizingMaskIntoConstraints = false
    heroImage.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    heroImage.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    heroImage.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    heroImage.heightAnchor.constraint(lessThanOrEqualTo: self.widthAnchor).isActive = true

    activityName.translatesAutoresizingMaskIntoConstraints = false
    activityName.topAnchor.constraint(equalTo: heroImage.bottomAnchor, constant: 12).isActive = true
    activityName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    activityName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    
    activityDescription.translatesAutoresizingMaskIntoConstraints = false
    activityDescription.topAnchor.constraint(equalTo: activityName.bottomAnchor, constant: 12).isActive = true
    activityDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    activityDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    
    activityCategory.translatesAutoresizingMaskIntoConstraints = false
    activityCategory.topAnchor.constraint(equalTo: activityDescription.bottomAnchor, constant: 16).isActive = true
    activityCategory.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    activityCategory.rightAnchor.constraint(equalTo: ageRange.leftAnchor, constant: -8).isActive = true
    
    ageRange.translatesAutoresizingMaskIntoConstraints = false
    ageRange.topAnchor.constraint(equalTo: activityDescription.bottomAnchor, constant: 16).isActive = true
    ageRange.leftAnchor.constraint(equalTo: activityCategory.rightAnchor, constant: 8).isActive = true
    ageRange.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    
    dateAndTime.translatesAutoresizingMaskIntoConstraints = false
    dateAndTime.topAnchor.constraint(equalTo: activityCategory.bottomAnchor, constant: 16).isActive = true
    dateAndTime.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    dateAndTime.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    
    location.translatesAutoresizingMaskIntoConstraints = false
    location.topAnchor.constraint(equalTo: dateAndTime.bottomAnchor, constant: 16).isActive = true
    location.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 16).isActive = true
    location.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
    location.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant:-16).isActive = true
  }
}
