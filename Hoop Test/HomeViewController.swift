//
//  HomeViewController.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

class HomeViewController : UITableViewController {
  fileprivate let reuseIdentifier = "HomeTableViewCellReuse"
  
  var coordinatorDelegate: AppCoordinatorDelegate?
  
  var activities: Array<Activity>? {
    didSet {
      activities?.sort(by: { (first, second) -> Bool in
        //order by start of activity, with the earliest activity first
        first.startDate.compare(second.startDate) == .orderedAscending
      })
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }
  
  init() {
    super.init(style: .plain)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func viewDidLoad() {
    self.view.backgroundColor = .white
    self.title = "Activities"
    self.tableView.register(HomeTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    self.tableView.rowHeight = UITableViewAutomaticDimension
    self.tableView.estimatedRowHeight = 120
    self.tableView.separatorStyle = .none
  }
}

// MARK:- UITableView methods
extension HomeViewController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return activities != nil ? activities!.count : 0
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! HomeTableViewCell
    cell.activity = (activities?[indexPath.row])!
    cell.cardColour = colourForCard(at: indexPath.row)
    cell.selectionStyle = .none
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    coordinatorDelegate?.didSelect((activities?[indexPath.row])!)
  }
}

// MARK:- View tools
extension HomeViewController {
  func colourForCard(at position: Int) -> UIColor {
    switch position % 4 {
    case 0:
      return Colours.green
    case 1:
      return Colours.blue
    case 2:
      return Colours.red
    case 3:
      return Colours.yellow
    default:
      return .white
    }
  }
}
