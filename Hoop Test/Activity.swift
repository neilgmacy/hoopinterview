//
//  Activity.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import Foundation

struct Activity {
  let name: String
  let ageRange: String
  let startDate: Date
  let endDate: Date
  let locationName: String
  let imageThumbnailUrlString: String
  let category: String
  let description: String
  let address: Address
  
  private let dateIntervalFormatter: DateIntervalFormatter = {
    let formatter = DateIntervalFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .short
    return formatter
  }()
  
  var dateTimeString: String {
    get {
      return dateIntervalFormatter.string(from: startDate, to: endDate)
    }
  }
  
  init?(json: [String: Any]) {
    //hard to know how much of the body of an activity is optional, so for the moment I'm assuming none of it is
    guard let name = json["name"] as? String,
      let ages = json["ages"] as? String,
      let startDateString = json["startDate"] as? String,
      let endDateString = json["endDate"] as? String,
      let placeName = json["placeName"] as? String,
      let imageURL = json["imageURL"] as? String,
      let category = json["category"] as? String,
      let description = json["description"] as? String,
      let address = Address(json: json["address"] as! [String : Any])
      else {
        return nil
    }
    
    let iso8601DateFormatter = ISO8601DateFormatter()
    guard let startDate = iso8601DateFormatter.date(from: startDateString) as Date! else { return nil }
    guard let endDate = iso8601DateFormatter.date(from: endDateString) as Date! else { return nil }
    
    self.name = name
    self.ageRange = ages
    self.startDate = startDate
    self.endDate = endDate
    self.locationName = placeName
    self.imageThumbnailUrlString = imageURL
    self.category = category
    self.description = description
    self.address = address
  }
}
