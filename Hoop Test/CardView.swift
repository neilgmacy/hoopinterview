//
//  CardView.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 14/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

class CardView : UIView {
  var colour: UIColor? {
    didSet {
      self.layer.backgroundColor = self.colour?.cgColor
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    self.layer.shadowOpacity = 0.2
    self.layer.shadowRadius = 1.0
    self.layer.shadowOffset = CGSize(width: 0, height: 2)
    self.layer.cornerRadius = 8.0
  }
}
