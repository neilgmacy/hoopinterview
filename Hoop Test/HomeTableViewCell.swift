//
//  HomeTableViewCell.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

class HomeTableViewCell : UITableViewCell {
  
  var activity: Activity! {
    didSet {
      activityName.text = activity.name
      ageRange.text = activity.ageRange
      dateAndTime.text = activity.dateTimeString
      location.text = activity.locationName
      
      let thumbnailUrlString = activity.imageThumbnailUrlString
      guard let thumbnailUrl = URL(string: thumbnailUrlString) else {
        return
      }
      thumbnail.downloadImage(url: thumbnailUrl)
    }
  }
  
  var cardColour: UIColor = .white {
    didSet {
      background.colour = cardColour
    }
  }
  
  let background = CardView()
  
  private let activityName: UILabel = {
    let label = UILabel()
    label.textColor = .white
    label.font = .preferredFont(forTextStyle: .title3)
    label.numberOfLines = 0
    return label
  }()
  
  private let ageRange: UILabel = {
    let label = UILabel()
    label.textColor = .white
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let dateAndTime: UILabel = {
    let label = UILabel()
    label.textColor = .white
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let location: UILabel = {
    let label = UILabel()
    label.textColor = .white
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let thumbnail: UIImageView = {
    let imageView = UIImageView()
    imageView.contentMode = .scaleAspectFit
    return imageView
  }()
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    contentView.addSubview(background)
    contentView.addSubview(activityName)
    contentView.addSubview(ageRange)
    contentView.addSubview(dateAndTime)
    contentView.addSubview(location)
    contentView.addSubview(thumbnail)
  
    //AutoLayout constraints
    background.translatesAutoresizingMaskIntoConstraints = false
    background.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8).isActive = true
    background.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8).isActive = true
    background.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8).isActive = true
    background.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8).isActive = true
    
    activityName.translatesAutoresizingMaskIntoConstraints = false
    activityName.topAnchor.constraint(equalTo: background.topAnchor, constant: 16).isActive = true
    activityName.leftAnchor.constraint(equalTo: background.leftAnchor, constant: 16).isActive = true
    activityName.rightAnchor.constraint(equalTo: thumbnail.leftAnchor, constant: -8).isActive = true
    
    ageRange.translatesAutoresizingMaskIntoConstraints = false
    ageRange.topAnchor.constraint(equalTo: activityName.bottomAnchor, constant: 8).isActive = true
    ageRange.leftAnchor.constraint(equalTo: background.leftAnchor, constant: 16).isActive = true
    ageRange.rightAnchor.constraint(equalTo: thumbnail.leftAnchor, constant: -8).isActive = true
    
    dateAndTime.translatesAutoresizingMaskIntoConstraints = false
    dateAndTime.topAnchor.constraint(equalTo: ageRange.bottomAnchor, constant: 8).isActive = true
    dateAndTime.leftAnchor.constraint(equalTo: background.leftAnchor, constant: 16).isActive = true
    dateAndTime.rightAnchor.constraint(equalTo: thumbnail.leftAnchor, constant: -8).isActive = true
    
    location.translatesAutoresizingMaskIntoConstraints = false
    location.topAnchor.constraint(equalTo: dateAndTime.bottomAnchor, constant: 8).isActive = true
    location.leftAnchor.constraint(equalTo: background.leftAnchor, constant: 16).isActive = true
    location.rightAnchor.constraint(equalTo: thumbnail.leftAnchor, constant: -8).isActive = true
    location.bottomAnchor.constraint(equalTo: background.bottomAnchor, constant: -16).isActive = true
    
    thumbnail.translatesAutoresizingMaskIntoConstraints = false
    thumbnail.rightAnchor.constraint(equalTo: background.rightAnchor, constant: -16).isActive = true
    thumbnail.centerYAnchor.constraint(equalTo: background.centerYAnchor).isActive = true
    thumbnail.widthAnchor.constraint(equalToConstant: 100).isActive = true
    thumbnail.heightAnchor.constraint(equalToConstant: 100).isActive = true
  }
}
