//
//  ActivityProvider.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import Foundation

//this could be expanded to also deal with local storage and/or a local cache
class ActivityProvider {
  
  private let networkActivityProvider = NetworkActivityProvider()
  
  func refreshActivities(_ finishedLoading: @escaping (_ activities: Array<Activity>?) -> Void) {
    networkActivityProvider.loadData(finishedLoading)
  }
}
