//
//  AppDelegate.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var appCoordinator: AppCoordinator?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    self.window = UIWindow(frame: UIScreen.main.bounds)
    
    let navController = UINavigationController()
    self.window!.rootViewController = navController
    
    self.appCoordinator = AppCoordinator(with: navController)
    appCoordinator!.start()
    
    self.window!.backgroundColor = .white
    self.window!.makeKeyAndVisible()
    return true
  }
}

