//
//  UIView_extensions.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

// Extensions to add useful and repeated functionality to UIImageView
extension UIImageView {
  ///Load an image from the given URL into this view
  func downloadImage(url: URL) {
    URLSession.shared.dataTask(with: url) { (data, response, error)  in
      guard let data = data, error == nil else { return }
      DispatchQueue.main.async() { () -> Void in
        self.image = UIImage(data: data)
      }
    }.resume()
  }
}
