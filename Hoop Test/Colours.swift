//
//  Colours.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 14/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit

struct Colours {
  static let red = UIColor(red: 0.9843, green: 0.2941, blue: 0.3249, alpha: 1.0)
  static let green = UIColor(red: 0.2745, green: 0.7411, blue: 0.6235, alpha: 1.0)
  static let blue = UIColor(red: 0.0, green: 0.5882, blue: 1.0, alpha: 1.0)
  static let yellow = UIColor(red: 0.9608, green: 0.7294, blue: 0.2941, alpha: 1.0)
}
