//
//  LocationView.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 14/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit
import MapKit

class LocationView: UIView {
  
  var coordinatorDelegate: AppCoordinatorDelegate?
  
  var activity: Activity! {
    didSet {
      address.text = "\(activity.locationName)\n\(activity.address.formattedAddress)"
      
      let coordinates = CLLocationCoordinate2D(latitude: activity.address.latitude, longitude:activity.address.longitude)
      map.setCenter(coordinates, animated: true)
      let regionDistance: CLLocationDistance = 1000
      let region = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
      map.setRegion(region, animated: true)
      map.addAnnotation(MKPlacemark(coordinate: coordinates))
    }
  }

  private let address: UILabel = {
    let label = UILabel()
    label.textColor = .darkGray
    label.font = .preferredFont(forTextStyle: .body)
    label.numberOfLines = 0
    return label
  }()
  
  private let map: MKMapView = {
    let mapview = MKMapView()
    mapview.isPitchEnabled = false
    mapview.isRotateEnabled = false
    mapview.isScrollEnabled = false
    mapview.isZoomEnabled = false
    return mapview
  }()
  
  private let openInMaps: UIButton = {
    let button = UIButton()
    button.setTitle("View in Maps", for: .normal)
    button.setTitleColor(Colours.blue, for: .normal)
    return button
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  private func commonInit() {
    self.addSubview(address)
    self.addSubview(map)
    self.addSubview(openInMaps)
    
    openInMaps.addTarget(self, action: #selector(openMapForActivity), for: .touchUpInside)
    
    //AutoLayout constraints
    address.translatesAutoresizingMaskIntoConstraints = false
    address.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    address.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    address.rightAnchor.constraint(equalTo: map.leftAnchor, constant: -8).isActive = true
    
    map.translatesAutoresizingMaskIntoConstraints = false
    map.centerYAnchor.constraint(equalTo: address.centerYAnchor).isActive = true
    map.leftAnchor.constraint(equalTo: address.rightAnchor, constant: 8).isActive = true
    map.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    map.widthAnchor.constraint(equalToConstant: 100).isActive = true
    map.heightAnchor.constraint(equalToConstant: 100).isActive = true
    
    openInMaps.translatesAutoresizingMaskIntoConstraints = false
    openInMaps.topAnchor.constraint(equalTo: address.bottomAnchor, constant: 8).isActive = true
    openInMaps.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
    openInMaps.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
  }
}

// MARK:- user actions
extension LocationView {
  @objc fileprivate func openMapForActivity() {
    coordinatorDelegate?.showLocationInMaps(activity)
  }
}
