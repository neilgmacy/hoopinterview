//
//  AppCoordinatorDelegate.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import Foundation

protocol AppCoordinatorDelegate {
  func didSelect(_ activity: Activity)
  
  //should maybe be a different coordinator, not sure this counts as core app routing!
  func showLocationInMaps(_ activity: Activity)
}
