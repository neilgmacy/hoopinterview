//
//  AppCoordinator.swift
//  Hoop Test
//
//  Created by Neil McGuiggan on 13/11/2016.
//  Copyright © 2016 Neil McGuiggan. All rights reserved.
//

import UIKit
import MapKit

///Top level application routing
class AppCoordinator {
  fileprivate let navigationController: UINavigationController
  fileprivate let activityProvider = ActivityProvider()
  
  init(with navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    //a more complex version of the app could have logic here to show login or onboarding
    self.showHomeView()
  }
}

// MARK:- Core functionality
extension AppCoordinator {
  fileprivate func showHomeView() {
    let homeViewController = HomeViewController()
    homeViewController.coordinatorDelegate = self
    self.navigationController.pushViewController(homeViewController, animated: true)
    activityProvider.refreshActivities({activities in
      homeViewController.activities = activities
    })
  }
}

// MARK:- AppCoordinatorDelegate
extension AppCoordinator : AppCoordinatorDelegate {
  func didSelect(_ activity: Activity) {
    let activityViewController = ActivityViewController()
    activityViewController.activity = activity
    activityViewController.coordinatorDelegate = self
    navigationController.pushViewController(activityViewController, animated: true)
  }
  
  func showLocationInMaps(_ activity: Activity) {
    let coordinates = CLLocationCoordinate2DMake(activity.address.latitude, activity.address.longitude)
    let regionDistance: CLLocationDistance = 5000
    let region = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
    let options = [
      MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: region.center),
      MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: region.span)
    ]
    let placemark = MKPlacemark(coordinate: coordinates)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = "\(activity.locationName)"
    mapItem.openInMaps(launchOptions: options)
  }
}
